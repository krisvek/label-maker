﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;


namespace Label_Maker
{
    public partial class Form1 : Form
    {
        //Strings used for input and output files
        String sInFile, sOutFile;

        Excel.Application excelApp;
        Excel.Workbook inWb;
        Excel.Worksheet inWs;            
        Excel.Workbook outWb;
        Excel.Worksheet outWs;
        Excel.Worksheet siteNamesAndOrdersSheet;
        Excel.Worksheet lambdaSheet;

        int lastRow;
        bool checkboxRack = false;
        
        List<string> siteNames = new List<string>();
        List<string> letterOrder = new List<string>();
        
        //Main user form
        public Form1()
        {
            InitializeComponent();

            toolTipHelp.SetToolTip(InFileButton, "Click this button to select the Excel worksheet (.xls) you wish to\r\n"+
                                                    "Generate labels for. The selected worksheet should be one that is\r\n"+
                                                    "exported from CTP's 'Internal Connections' report.");
            toolTipHelp.SetToolTip(textBoxInputFile, "This is the path to your selected Input File.");
            toolTipHelp.SetToolTip(checkBoxRacks, "Select this checkbox to generate your labels WITH Rack numbers.\r\n" +
                                                    "Deselect this checkbox to generate your labels WITHOUT Rack numbers.");
            toolTipHelp.SetToolTip(labelSiteName, "You can select a Site from the Input File here.\r\n" +
                                                        "With a Site selected, you may change the Order of the Shelves\r\n" +
                                                        "using either the drop-down lists or typing it in manually.");
            toolTipHelp.SetToolTip(comboBoxSiteNames, "You can select a Site from the Input File here.\r\n" +
                                                        "With a Site selected, you may change the Order of the Shelves\r\n" +
                                                        "using either the drop-down lists or typing it in manually.");            
            toolTipHelp.SetToolTip(labelShelfDefaults, "This is a pre-generated selection of Shelf Orders that you can\r\n" +
                                                            "select to apply to EVERY Site from the Input File.");
            toolTipHelp.SetToolTip(comboBoxShelfDefaults, "This is a pre-generated selection of Shelf Orders that you can\r\n" +
                                                            "select to apply to EVERY Site from the Input File.");
            toolTipHelp.SetToolTip(labelShelfOrder, "This shows you the Shelf Order for the currently selected Site.\r\n"+
                                                    "You may manually edit this field if necessary.");
            toolTipHelp.SetToolTip(textBoxShelfOrder, "This shows you the Shelf Order for the currently selected Site.\r\n" +
                                                    "You may manually edit this field if necessary.");
            toolTipHelp.SetToolTip(labelSiteDefaults, "This is a pre-generated selection of Shelf Orders that you can\r\n" +
                                                            "select to apply to the CURRENTLY SELECTED Site from the Input File.");
            toolTipHelp.SetToolTip(comboBoxSiteDefaults, "This is a pre-generated selection of Shelf Orders that you can\r\n" +
                                                            "select to apply to the CURRENTLY SELECTED Site from the Input File.");
            toolTipHelp.SetToolTip(OutFileButton, "Click this button to select or create the Excel workbook (.xlsx) that\r\n"+
                                                    "you wish to save the generated labels as.");            
            toolTipHelp.SetToolTip(textBoxOutputFile, "This is the path to your selected Output File.");
            toolTipHelp.SetToolTip(GenLabelButton, "Click this button to Generate the labels using the selected parameters above.\r\n"+
                                                    "The resulting workbook will be opened upon completion of the task.");
        }

        //Select the input file
        private void InFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofdInFile = new OpenFileDialog();
            ofdInFile.Filter = "Excel 97-2003 Workbooks (*.xls)|*.xls";
            ofdInFile.RestoreDirectory = true;
            if (ofdInFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                sInFile = ofdInFile.FileName;
                textBoxInputFile.Clear();
                textBoxInputFile.AppendText(sInFile);
            }
            
            if (sInFile != null)
            {

                this.Cursor = Cursors.WaitCursor; // shows the 'wait' cursor so the user knows things are gettin' busy
                //Create excel application
                excelApp = new Excel.Application();
                //excelApp.Visible = true; // useful for debugging Excel formulas
                excelApp.DisplayAlerts = false; // stops a number of wanrings from popping up, which delays the execution

                //Copy CTP export into new workbook so the original raw data doesn't get corrupted.
                inWb = excelApp.Workbooks.Open(sInFile, 0, false, 5, "", "", false, Excel.XlPlatform.xlWindows, "", true, false, 0, true, false, false);
                inWs = inWb.Worksheets[1];
                inWs.UsedRange.Copy(Type.Missing);

                outWb = excelApp.Workbooks.Add(Type.Missing);
                Excel.Range range = (Excel.Range)((Excel.Worksheet)outWb.Worksheets[1]).Cells[3,1];
                range.PasteSpecial(Excel.XlPasteType.xlPasteValues, Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, true, false);
                Clipboard.Clear();
                inWb.Close(0);
                
                outWs = outWb.Worksheets[1];
                outWs.Name = "Imported";
                outWs.Cells.Locked = false; 
                outWb.Worksheets.Add(Type.Missing, outWs);
                siteNamesAndOrdersSheet = outWb.Worksheets[2]; 
                siteNamesAndOrdersSheet.Name = "Site Names and Orders";
                lambdaSheet = outWb.Worksheets[3];
                lambdaSheet.Name = "Lambdas";
                populateLambdas(lambdaSheet);   //Create Wavelength to Channel Number sheet

                Excel.Range last1 = outWs.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
                lastRow = last1.Row;

                Excel.Range sitesRange = outWs.get_Range("A4", "A" + lastRow);
                sitesRange.AutoFilter(1, "<>", Excel.XlAutoFilterOperator.xlAnd, Type.Missing, true);
                Excel.Range visibleCells = sitesRange.SpecialCells(Excel.XlCellType.xlCellTypeVisible, Type.Missing);
                visibleCells.Locked = true;

                outWs.AutoFilterMode = false;
                outWs.UsedRange.Rows.Hidden = false;
                outWs.UsedRange.Columns.Hidden = false;
                outWs.Protect("", Type.Missing, Type.Missing, Type.Missing, false, true, true, true, true, true, true, true, true, 
                                                                                                            true, true, true);
                                
                for (int area_Id = 1; area_Id <= visibleCells.Areas.Count; area_Id++)
                {
                    siteNames.Add(visibleCells.Areas.get_Item(area_Id).Value);
                    letterOrder.Add("blank");
                }
                populateSiteNamesAndOrders(siteNamesAndOrdersSheet);

                comboBoxShelfDefaults.Items.AddRange(new string[] { "ABCDEFGH", "AABBCCDD", "ABCD", "ACBD" });
                comboBoxSiteDefaults.Items.AddRange(new string[] { "ABCDEFGH", "AABBCCDD", "ABCD", "ACBD", "AB", "A" });
                comboBoxSiteNames.Items.AddRange(siteNames.ToArray());
                comboBoxSiteNames.SelectedIndex = 0;                
                comboBoxShelfDefaults.SelectedIndex = 0;
                comboBoxSiteDefaults.SelectedIndex = 0;

                labelSiteName.Visible = true;
                comboBoxSiteNames.Visible = true;
                checkBoxRacks.Visible = true;
                labelShelfDefaults.Visible = true;
                comboBoxShelfDefaults.Visible = true;
                labelSiteDefaults.Visible = true;
                comboBoxSiteDefaults.Visible = true;
                labelShelfOrder.Visible = true;                
                textBoxShelfOrder.Visible = true;
                textBoxShelfOrder.Enabled = true;                
                GenLabelButton.Visible = true;                

                this.Cursor = Cursors.Default;
            }
            if (sInFile != null && sOutFile != null)
                GenLabelButton.Enabled = true;
        }

        //Select the output file
        private void OutFileButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfdOutFile = new SaveFileDialog();
            sfdOutFile.AddExtension = true;
            sfdOutFile.Filter = "Excel Workbooks (*.xlsx)|*.xlsx";
            sfdOutFile.RestoreDirectory = true;
            sfdOutFile.OverwritePrompt = false;
            if (sfdOutFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                sOutFile = sfdOutFile.FileName;
                textBoxOutputFile.Clear();
                textBoxOutputFile.AppendText(sOutFile);
            }
            if (sInFile != null && sOutFile != null)
                GenLabelButton.Enabled = true;
        }

        //Generate the labels
        private void GenLabelButton_Click(object sender, EventArgs e)
        {
            if (sInFile == null || sOutFile == null)
                System.Windows.Forms.MessageBox.Show("Input or Output file not set. This shouldn't be possible.");

            this.Cursor = Cursors.WaitCursor;
                        
            excelApp.DisplayAlerts = false;

            preFormat(outWb, outWs);        //Various formatting that must occur before populating formulas
            findAndReplace(outWs);          //Replaces textual items to shorten label text length
            populateFormulas(outWs);        //Populates the excel formulas that do the heavy work
            formatSheet(outWs);             //Post-label-creation formatting
            createFinalForm(outWb, outWs);  //create the final worksheet ready for ordering
            excelApp.DisplayAlerts = true;

            //Clean up
            outWb.SaveAs(sOutFile);  //TODO:crashed when user said do not overwrite
            excelApp.Visible = true;

            sInFile = null;
            sOutFile = null;
            textBoxInputFile.Clear();
            textBoxOutputFile.Clear();
            inWb = null;
            inWs = null;
            outWs = null;
            siteNamesAndOrdersSheet = null;
            lambdaSheet = null;
            lastRow = 0;
            siteNames.Clear();
            letterOrder.Clear();
            comboBoxSiteNames.Items.Clear();
            comboBoxSiteDefaults.Items.Clear();
            comboBoxShelfDefaults.Items.Clear();
            labelSiteName.Visible = false;
            comboBoxSiteNames.Visible = false;
            checkBoxRacks.Visible = false;
            labelShelfDefaults.Visible = false;
            comboBoxShelfDefaults.Visible = false;
            labelSiteDefaults.Visible = false;
            comboBoxSiteDefaults.Visible = false;
            labelShelfOrder.Visible = false;
            textBoxShelfOrder.Visible = false;
            textBoxShelfOrder.Enabled = false;
            GenLabelButton.Visible = false;
            GenLabelButton.Enabled = false;

            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(outWb);
            //System.Runtime.InteropServices.Marshal.FinalReleaseComObject(inWb);
            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excelApp);

            //confirmation message
            //System.Windows.Forms.MessageBox.Show("Labels complete.  Stored in " + sOutFile);
            this.Cursor = Cursors.Default;
            //outWb.Close(0);
            //excelApp.Quit();
        }

        private void populateSiteNamesAndOrders(Excel.Worksheet siteNamesAndOrdersSheet)
        {
           for (int i = 1; i <= siteNames.Count; i++)
            {
                siteNamesAndOrdersSheet.Range["A" + i.ToString() + ""].Value = siteNames[i - 1].ToString();
                siteNamesAndOrdersSheet.Range["B" + i.ToString() + ""].Value = letterOrder[i - 1].ToString();
            }
        }

        //Create sheet that has channel mapping to wavelengths
        private void populateLambdas(Excel.Worksheet lambdaSheet)
        {
            lambdaSheet.Range["A1"].Value = "CHAN-1"; lambdaSheet.Range["B1"].Value = "1530.33";
            lambdaSheet.Range["A2"].Value = "CHAN-2"; lambdaSheet.Range["B2"].Value = "1531.12";
            lambdaSheet.Range["A3"].Value = "CHAN-3"; lambdaSheet.Range["B3"].Value = "1531.90";
            lambdaSheet.Range["A4"].Value = "CHAN-4"; lambdaSheet.Range["B4"].Value = "1532.68";
            lambdaSheet.Range["A5"].Value = "CHAN-5"; lambdaSheet.Range["B5"].Value = "1533.47";
            lambdaSheet.Range["A6"].Value = "CHAN-6"; lambdaSheet.Range["B6"].Value = "1534.25";
            lambdaSheet.Range["A7"].Value = "CHAN-7"; lambdaSheet.Range["B7"].Value = "1535.04";
            lambdaSheet.Range["A8"].Value = "CHAN-8"; lambdaSheet.Range["B8"].Value = "1535.82";
            lambdaSheet.Range["A9"].Value = "CHAN-9"; lambdaSheet.Range["B9"].Value = "1536.61";
            lambdaSheet.Range["A10"].Value = "CHAN-10"; lambdaSheet.Range["B10"].Value = "1537.40";
            lambdaSheet.Range["A11"].Value = "CHAN-11"; lambdaSheet.Range["B11"].Value = "1538.19";
            lambdaSheet.Range["A12"].Value = "CHAN-12"; lambdaSheet.Range["B12"].Value = "1538.98";
            lambdaSheet.Range["A13"].Value = "CHAN-13"; lambdaSheet.Range["B13"].Value = "1539.77";
            lambdaSheet.Range["A14"].Value = "CHAN-14"; lambdaSheet.Range["B14"].Value = "1540.56";
            lambdaSheet.Range["A15"].Value = "CHAN-15"; lambdaSheet.Range["B15"].Value = "1541.35";
            lambdaSheet.Range["A16"].Value = "CHAN-16"; lambdaSheet.Range["B16"].Value = "1542.14";
            lambdaSheet.Range["A17"].Value = "CHAN-17"; lambdaSheet.Range["B17"].Value = "1542.94";
            lambdaSheet.Range["A18"].Value = "CHAN-18"; lambdaSheet.Range["B18"].Value = "1543.73";
            lambdaSheet.Range["A19"].Value = "CHAN-19"; lambdaSheet.Range["B19"].Value = "1544.53";
            lambdaSheet.Range["A20"].Value = "CHAN-20"; lambdaSheet.Range["B20"].Value = "1545.32";
            lambdaSheet.Range["A21"].Value = "CHAN-21"; lambdaSheet.Range["B21"].Value = "1546.12";
            lambdaSheet.Range["A22"].Value = "CHAN-22"; lambdaSheet.Range["B22"].Value = "1546.92";
            lambdaSheet.Range["A23"].Value = "CHAN-23"; lambdaSheet.Range["B23"].Value = "1547.72";
            lambdaSheet.Range["A24"].Value = "CHAN-24"; lambdaSheet.Range["B24"].Value = "1548.51";
            lambdaSheet.Range["A25"].Value = "CHAN-25"; lambdaSheet.Range["B25"].Value = "1549.32";
            lambdaSheet.Range["A26"].Value = "CHAN-26"; lambdaSheet.Range["B26"].Value = "1550.12";
            lambdaSheet.Range["A27"].Value = "CHAN-27"; lambdaSheet.Range["B27"].Value = "1550.92";
            lambdaSheet.Range["A28"].Value = "CHAN-28"; lambdaSheet.Range["B28"].Value = "1551.72";
            lambdaSheet.Range["A29"].Value = "CHAN-29"; lambdaSheet.Range["B29"].Value = "1552.52";
            lambdaSheet.Range["A30"].Value = "CHAN-30"; lambdaSheet.Range["B30"].Value = "1553.33";
            lambdaSheet.Range["A31"].Value = "CHAN-31"; lambdaSheet.Range["B31"].Value = "1554.13";
            lambdaSheet.Range["A32"].Value = "CHAN-32"; lambdaSheet.Range["B32"].Value = "1554.94";
            lambdaSheet.Range["A33"].Value = "CHAN-33"; lambdaSheet.Range["B33"].Value = "1555.75";
            lambdaSheet.Range["A34"].Value = "CHAN-34"; lambdaSheet.Range["B34"].Value = "1556.55";
            lambdaSheet.Range["A35"].Value = "CHAN-35"; lambdaSheet.Range["B35"].Value = "1557.36";
            lambdaSheet.Range["A36"].Value = "CHAN-36"; lambdaSheet.Range["B36"].Value = "1558.17";
            lambdaSheet.Range["A37"].Value = "CHAN-37"; lambdaSheet.Range["B37"].Value = "1558.98";
            lambdaSheet.Range["A38"].Value = "CHAN-38"; lambdaSheet.Range["B38"].Value = "1559.79";
            lambdaSheet.Range["A39"].Value = "CHAN-39"; lambdaSheet.Range["B39"].Value = "1560.61";
            lambdaSheet.Range["A40"].Value = "CHAN-40"; lambdaSheet.Range["B40"].Value = "1561.42";
        }

        //Miscellaneous deletions that must occur before populating formulas
        private void preFormat(Excel.Workbook outWb, Excel.Worksheet outWs)
        {
            //Create named range for WLENS (For Formulas)
            outWb.Names.Add("WLENS", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, "=Lambdas!R1C1:R40C2", Type.Missing);
            outWb.Names.Add("SITES", Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, "='Site Names and Orders'!R1C1:R15C2", Type.Missing);

            Excel.Range blankA = outWs.get_Range("A5", "A"+lastRow);
            try
            {
                blankA.Formula = "=R[-1]C";
            }
            catch { }
                ;
            //Delete Columns J & F
            outWs.Range["J1"].EntireColumn.Delete();
            outWs.Range["F1"].EntireColumn.Delete();
        }

        //Change some text fields to shorten the length of the labels
        private void findAndReplace(Excel.Worksheet outSheet)
        {
            //TODO: Suppress dialog that pops up when the search term isn't found in the sheet
            //TODO: Populate this list with all different types of textual changes

            outSheet.Cells.Replace("MD-40", "MD40");
            outSheet.Cells.Replace("EF-40", "EF40");
            outSheet.Cells.Replace("PP-4-SMR", "PP4SMR"); // v1.2
            outSheet.Cells.Replace("PP-MESH-4", "PP4MESH"); // v1.2
            outSheet.Cells.Replace("PP-MESH-8", "PP8MESH"); // v1.1
            outSheet.Cells.Replace("80-WXC", "80WXC"); // v1.1            
            outSheet.Cells.Replace("TNC-E", "TNC"); // only one type of TNC per chassis
            outSheet.Cells.Replace("40-SMR1-C", "SMR1");
            outSheet.Cells.Replace("40-SMR2-C", "SMR2");
            outSheet.Cells.Replace("OPT-AMP-17", "AMP17");
            outSheet.Cells.Replace("OTU2-XP", "OTU2");
            outSheet.Cells.Replace("15454-AR-XP", "ARXP");
            outSheet.Cells.Replace("15454-AR-MXP", "ARMXP");
            outSheet.Cells.Replace("15454-OPT-EDFA-24", "EDFA24"); // v1.2
            outSheet.Cells.Replace("15454-OPT-EDFA-17", "EDFA17"); // v1.21
            outSheet.Cells.Replace("15216-MD-ID-50", "MD_ID50"); // v1.2
            outSheet.Cells.Replace(" TX", "_TX");
            outSheet.Cells.Replace("-TX", "_TX");
            outSheet.Cells.Replace(" RX", "_RX");
            outSheet.Cells.Replace("-RX", "_RX");
            outSheet.Cells.Replace(" - ", "-"); // v1.2
            outSheet.Cells.Replace("OPT-AMP-C", "AMPC");
            outSheet.Cells.Replace("OPT-RAMP-C", "RAMPC");            
            outSheet.Cells.Replace("OPT-PRE","PRE");
            outSheet.Cells.Replace("OSC-CSM", "OSCCSM");
            outSheet.Cells.Replace("100G-LC-C", "100G"); //only one version of this card for now
            outSheet.Cells.Replace("RAMAN-CTP", "RAMAN"); //are there more of these?
        }

        //Places formulas in cells to do heavy lifting
        private void populateFormulas(Excel.Worksheet outSheet)
        {
            string columnNEAR, columnFAR;
            //TODO: Figure out which DCU is a-side or B-side
            //Formulas that do the heavy lifting

            if (checkboxRack == true)
            {
                columnNEAR = "\"R\",MID(C4,6,1),\"-\",";
                columnFAR = "\"R\",MID(F4,6,1),\"-\",";
            }
            else
            {
                columnNEAR = "";
                columnFAR = "";
            }

            // NEAR END
            outSheet.Range["I4"].Value = "=IF(ISNUMBER(SEARCH(\"MD40\",C4)),CONCATENATE(" + columnNEAR + "D4,\"-\"," +
                                            "IF(ISNUMBER(SEARCH(\"SHELF 1\",C4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),1,1),\"-\"),"+
                                            "IF(ISNUMBER(SEARCH(\"SHELF 2\",C4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),2,1),\"-\"),"+
                                            "IF(ISNUMBER(SEARCH(\"SHELF 3\",C4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),3,1),\"-\")," +
                                            "IF(ISNUMBER(SEARCH(\"SHELF 4\",C4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),4,1),\"-\")," +
                                            "IF(ISNUMBER(SEARCH(\"SHELF 5\",C4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),5,1),\"-\")," +
                                            "IF(ISNUMBER(SEARCH(\"SHELF 6\",C4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),6,1),\"-\")," +
                                            "IF(ISNUMBER(SEARCH(\"SHELF 7\",C4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),7,1),\"-\")," +
                                                                              "CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),8,1),\"-\")))))))),"+
                                            "IFERROR(CONCATENATE(VLOOKUP(LEFT(E4,LEN(E4)-3),WLENS,2,FALSE),RIGHT(E4,3)),E4)),"+
                                            "IF(ISNUMBER(SEARCH(\"DCU\",D4)),CONCATENATE(" + columnNEAR + "D4,\"-\",E4)," +
                                            "IF(ISNUMBER(SEARCH(\"PP4SMR\",D4)),CONCATENATE(" + columnNEAR + "D4,\"-\",E4)," +
                                            "IF(ISNUMBER(SEARCH(\"PP4MESH\",D4)),CONCATENATE(" + columnNEAR + "D4,\"-\",E4)," +
                                            "IF(ISNUMBER(SEARCH(\"PP8MESH\",D4)),CONCATENATE(" + columnNEAR + "D4,\"-\",E4)," +
                    "CONCATENATE(" + columnNEAR + "\"SH\",MID(C4,SEARCH(\".Slot\",C4)-1,1),\"-SL\",RIGHT(C4,1),\"-\",D4,\"-\",E4))))))";
            // FAR END
            outSheet.Range["J4"].Value = "=IF(ISNUMBER(SEARCH(\"MD40\",F4)),CONCATENATE(" + columnFAR + "G4,\"-\"," +
                                            "IF(ISNUMBER(SEARCH(\"SHELF 1\",F4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),1,1),\"-\"),"+
                                            "IF(ISNUMBER(SEARCH(\"SHELF 2\",F4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),2,1),\"-\"),"+
                                            "IF(ISNUMBER(SEARCH(\"SHELF 3\",F4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),3,1),\"-\")," +
                                            "IF(ISNUMBER(SEARCH(\"SHELF 4\",F4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),4,1),\"-\")," +
                                            "IF(ISNUMBER(SEARCH(\"SHELF 5\",F4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),5,1),\"-\")," +
                                            "IF(ISNUMBER(SEARCH(\"SHELF 6\",F4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),6,1),\"-\")," +
                                            "IF(ISNUMBER(SEARCH(\"SHELF 7\",F4)),CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),7,1),\"-\")," +
                                                                             "CONCATENATE(MID(VLOOKUP(A4,SITES,2,FALSE),8,1),\"-\")))))))),"+
                                            "IFERROR(CONCATENATE(VLOOKUP(LEFT(H4,LEN(H4)-3),WLENS,2,FALSE),RIGHT(H4,3)),H4)),"+
                                            "IF(ISNUMBER(SEARCH(\"DCU\",G4)),CONCATENATE(" + columnFAR + "G4,\"-\",H4)," +
                                            "IF(ISNUMBER(SEARCH(\"PP4SMR\",G4)),CONCATENATE(" + columnFAR + "G4,\"-\",H4)," +
                                            "IF(ISNUMBER(SEARCH(\"PP4MESH\",G4)),CONCATENATE(" + columnFAR + "G4,\"-\",H4)," +
                                            "IF(ISNUMBER(SEARCH(\"PP8MESH\",G4)),CONCATENATE(" + columnFAR + "G4,\"-\",H4)," +
                    "CONCATENATE(" + columnFAR + "\"SH\",MID(F4,SEARCH(\".Slot\",F4)-1,1),\"-SL\",MID(F4,SEARCH(\".Slot\",F4)+6,2),\"-\",G4,\"-\",H4))))))";
            
            outSheet.Range["K4"].Value = "=IFERROR(CONCATENATE(\"NE:\",I4,CHAR(10),"+
                                                              "\"FE:\",J4),IF(OR(ISNUMBER(FIND(\"Rack\",C4)),ISNUMBER(FIND(\"Fiber\",B4))),\"\",A4))";
            outSheet.Range["L4"].Value = "=IFERROR(CONCATENATE(\"NE:\",J4,CHAR(10),"+
                                                              "\"FE:\",I4),\"\")";
            
            //Fill down the formulas
            outWs.Unprotect();
            Excel.Range last = outSheet.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell, Type.Missing);
            Excel.Range range = outSheet.get_Range("B1", last);
            lastRow = last.Row;
            outSheet.Range["I4", "I" + lastRow].FillDown();
            outSheet.Range["J4", "J" + lastRow].FillDown();
            outSheet.Range["K4", "K" + lastRow].FillDown();
            outSheet.Range["L4", "L" + lastRow].FillDown();
        }

        //Post label generation formatting
        private void formatSheet(Excel.Worksheet outSheet)
        {
            //Stretch the columns and word wrap the text fields
            outSheet.Range["K1", "L1000"].Font.Name = "Consolas";
            outSheet.Range["K1", "L1000"].Columns.AutoFit();
            outSheet.Range["K1", "L1000"].WrapText = true;
            outSheet.Range["K1", "L1000"].Columns.AutoFit();
        }

        //Creates the final order form
        private void createFinalForm(Excel.Workbook outWb, Excel.Worksheet outWs)
        {
            int fiberNumber = 0;
            Excel.Worksheet finalSheet = outWb.Worksheets[4];//4
            finalSheet.Name = "Final Set";
            finalSheet.Activate();
           
            //copy data from original sheet to final sheet
            Excel.Range R1 = (Excel.Range)outWs.Range["K4", "L1000"];
            R1.Copy(Type.Missing);
            Excel.Range R2 = (Excel.Range)finalSheet.Range["M1", "N1000"];
            R2.PasteSpecial(Excel.XlPasteType.xlPasteValues, Excel.XlPasteSpecialOperation.xlPasteSpecialOperationNone, false, false);
            
            //Remove excess data
            finalSheet.Range["A1"].EntireColumn.Delete();
            finalSheet.Range["A1"].EntireColumn.Delete();
            finalSheet.Range["A1"].EntireColumn.Delete();
            finalSheet.Range["A1"].EntireColumn.Delete();
            finalSheet.Range["A1"].EntireColumn.Delete();
            finalSheet.Range["A1"].EntireColumn.Delete();
            finalSheet.Range["A1"].EntireColumn.Delete();
            finalSheet.Range["A1"].EntireColumn.Delete();
            finalSheet.Range["A1"].EntireColumn.Delete();
            finalSheet.Range["A1"].EntireColumn.Delete();
            finalSheet.Range["A1"].EntireColumn.Delete();
            finalSheet.Range["A1"].EntireColumn.Delete();

            //Formatting
            finalSheet.Range["A1", "B1000"].Font.Name = "Consolas";
            finalSheet.Range["A1", "B1000"].Font.Size = 11;
            finalSheet.Range["A1", "B1000"].Columns.AutoFit();
            finalSheet.Range["A1", "B1000"].WrapText = true;
            finalSheet.Range["A1", "B1000"].Columns.AutoFit();
            
            finalSheet.Range["A1"].EntireColumn.Insert();
            finalSheet.Range["A1"].EntireColumn.Font.Name = "Consolas";
            finalSheet.Range["A1"].EntireColumn.Font.Size = 11;
            finalSheet.Range["A1"].EntireColumn.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
            finalSheet.Range["A1"].EntireColumn.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
            //clear blanks & accentuate site names
            for (int i = 1; i <= lastRow; i++)
            {
                //clear out blanks
                if (finalSheet.Range["B" + i].Value == "" || finalSheet.Range["B" + i].Value == null)
                {
                    finalSheet.Range["B" + i].EntireRow.Delete();
                    i--;
                    lastRow--;
                }
                //create site names and fiber a end / z end headings
                else if (finalSheet.Range["C" + i].Value == "")
                {
                    finalSheet.Range["B" + i].EntireRow.Insert();
                    finalSheet.Range["B" + i].Value = finalSheet.Range["B" + (i + 1)].Value;
                    finalSheet.Range["B" + i].Font.Name = "Consolas";
                    finalSheet.Range["B" + i].Font.Size = 14;
                    finalSheet.Range["B" + i].Font.Bold = true;
                    finalSheet.Range["B" + i, "C" + i].Merge();
                    finalSheet.Range["B" + i, "C" + i].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    i++;
                    lastRow++;
                    finalSheet.Range["A" + i].Value = "Fiber #";
                    finalSheet.Range["B" + i].Value = "Fiber Label A End";
                    finalSheet.Range["C" + i].Value = "Fiber Label Z End";
                    finalSheet.Range["A" + i, "C" + i].Font.Size = 14;
                    finalSheet.Range["A" + i, "C" + i].Font.Bold = true;
                    finalSheet.Range["A" + i, "C" + i].Font.Name = "Consolas";
                    finalSheet.Range["B" + i, "C" + i].HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    fiberNumber = 0;
                }
                else
                {
                    fiberNumber++;
                    finalSheet.Range["A" + i].Value = fiberNumber;
                }
            }
            finalSheet.Range["A1", "C1000"].Columns.AutoFit();
            finalSheet.Range["A1", "C1000"].WrapText = true;
            finalSheet.Range["A1", "C1000"].Columns.AutoFit();
            finalSheet.Range["A1"].Select();
        }

        private void textBoxShelfOrder_TextChanged(object sender, EventArgs e)
        {
            int selectedIndex = comboBoxSiteNames.SelectedIndex;
            letterOrder[selectedIndex] = textBoxShelfOrder.Text;
            siteNamesAndOrdersSheet.Range["B" + (selectedIndex + 1)].Value = letterOrder[selectedIndex];
        }
        
        private void comboBoxSiteNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxShelfOrder.Text = letterOrder.ElementAt(comboBoxSiteNames.SelectedIndex);
        }

        private void comboBoxShelfDefaults_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < (letterOrder.Count); i++) // Loop through List with for
            {
                letterOrder[i] = comboBoxShelfDefaults.SelectedItem.ToString();
                siteNamesAndOrdersSheet.Range["B" + (i + 1)].Value = letterOrder[i];
            }
            textBoxShelfOrder.Text = letterOrder.ElementAt(comboBoxSiteNames.SelectedIndex);
        }

        private void comboBoxSiteDefaults_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxShelfOrder.Text = comboBoxSiteDefaults.SelectedItem.ToString();
        }

        private void checkBoxRacks_CheckedChanged(object sender, EventArgs e)
        {
            checkboxRack = checkBoxRacks.Checked;
        }
    }
}
