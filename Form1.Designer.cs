﻿namespace Label_Maker
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.InFileButton = new System.Windows.Forms.Button();
            this.OutFileButton = new System.Windows.Forms.Button();
            this.textBoxInputFile = new System.Windows.Forms.TextBox();
            this.textBoxOutputFile = new System.Windows.Forms.TextBox();
            this.GenLabelButton = new System.Windows.Forms.Button();
            this.labelSiteName = new System.Windows.Forms.Label();
            this.comboBoxSiteNames = new System.Windows.Forms.ComboBox();
            this.textBoxShelfOrder = new System.Windows.Forms.TextBox();
            this.labelShelfDefaults = new System.Windows.Forms.Label();
            this.comboBoxShelfDefaults = new System.Windows.Forms.ComboBox();
            this.checkBoxRacks = new System.Windows.Forms.CheckBox();
            this.labelShelfOrder = new System.Windows.Forms.Label();
            this.comboBoxSiteDefaults = new System.Windows.Forms.ComboBox();
            this.labelSiteDefaults = new System.Windows.Forms.Label();
            this.toolTipHelp = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // InFileButton
            // 
            this.InFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InFileButton.Location = new System.Drawing.Point(13, 12);
            this.InFileButton.Margin = new System.Windows.Forms.Padding(4);
            this.InFileButton.Name = "InFileButton";
            this.InFileButton.Size = new System.Drawing.Size(97, 27);
            this.InFileButton.TabIndex = 0;
            this.InFileButton.Text = "Input File";
            this.InFileButton.UseVisualStyleBackColor = true;
            this.InFileButton.Click += new System.EventHandler(this.InFileButton_Click);
            // 
            // OutFileButton
            // 
            this.OutFileButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.OutFileButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OutFileButton.Location = new System.Drawing.Point(13, 128);
            this.OutFileButton.Margin = new System.Windows.Forms.Padding(4);
            this.OutFileButton.Name = "OutFileButton";
            this.OutFileButton.Size = new System.Drawing.Size(97, 27);
            this.OutFileButton.TabIndex = 1;
            this.OutFileButton.Text = "Output File";
            this.OutFileButton.UseVisualStyleBackColor = true;
            this.OutFileButton.Click += new System.EventHandler(this.OutFileButton_Click);
            // 
            // textBoxInputFile
            // 
            this.textBoxInputFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxInputFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxInputFile.Location = new System.Drawing.Point(112, 13);
            this.textBoxInputFile.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxInputFile.Name = "textBoxInputFile";
            this.textBoxInputFile.ReadOnly = true;
            this.textBoxInputFile.Size = new System.Drawing.Size(625, 26);
            this.textBoxInputFile.TabIndex = 2;
            // 
            // textBoxOutputFile
            // 
            this.textBoxOutputFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOutputFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxOutputFile.Location = new System.Drawing.Point(112, 129);
            this.textBoxOutputFile.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxOutputFile.Name = "textBoxOutputFile";
            this.textBoxOutputFile.ReadOnly = true;
            this.textBoxOutputFile.Size = new System.Drawing.Size(864, 26);
            this.textBoxOutputFile.TabIndex = 3;
            // 
            // GenLabelButton
            // 
            this.GenLabelButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GenLabelButton.Enabled = false;
            this.GenLabelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GenLabelButton.Location = new System.Drawing.Point(13, 163);
            this.GenLabelButton.Margin = new System.Windows.Forms.Padding(4);
            this.GenLabelButton.Name = "GenLabelButton";
            this.GenLabelButton.Size = new System.Drawing.Size(963, 101);
            this.GenLabelButton.TabIndex = 4;
            this.GenLabelButton.Text = "Generate Labels";
            this.GenLabelButton.UseVisualStyleBackColor = true;
            this.GenLabelButton.Visible = false;
            this.GenLabelButton.Click += new System.EventHandler(this.GenLabelButton_Click);
            // 
            // labelSiteName
            // 
            this.labelSiteName.AutoSize = true;
            this.labelSiteName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSiteName.Location = new System.Drawing.Point(12, 51);
            this.labelSiteName.Name = "labelSiteName";
            this.labelSiteName.Size = new System.Drawing.Size(81, 18);
            this.labelSiteName.TabIndex = 5;
            this.labelSiteName.Text = "Site Name:";
            this.labelSiteName.Visible = false;
            // 
            // comboBoxSiteNames
            // 
            this.comboBoxSiteNames.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSiteNames.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSiteNames.FormattingEnabled = true;
            this.comboBoxSiteNames.Location = new System.Drawing.Point(112, 47);
            this.comboBoxSiteNames.Name = "comboBoxSiteNames";
            this.comboBoxSiteNames.Size = new System.Drawing.Size(339, 28);
            this.comboBoxSiteNames.TabIndex = 6;
            this.comboBoxSiteNames.Visible = false;
            this.comboBoxSiteNames.SelectedIndexChanged += new System.EventHandler(this.comboBoxSiteNames_SelectedIndexChanged);
            // 
            // textBoxShelfOrder
            // 
            this.textBoxShelfOrder.Enabled = false;
            this.textBoxShelfOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxShelfOrder.Location = new System.Drawing.Point(112, 92);
            this.textBoxShelfOrder.Name = "textBoxShelfOrder";
            this.textBoxShelfOrder.Size = new System.Drawing.Size(339, 26);
            this.textBoxShelfOrder.TabIndex = 16;
            this.textBoxShelfOrder.Visible = false;
            this.textBoxShelfOrder.TextChanged += new System.EventHandler(this.textBoxShelfOrder_TextChanged);
            // 
            // labelShelfDefaults
            // 
            this.labelShelfDefaults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelShelfDefaults.AutoSize = true;
            this.labelShelfDefaults.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShelfDefaults.Location = new System.Drawing.Point(512, 51);
            this.labelShelfDefaults.Name = "labelShelfDefaults";
            this.labelShelfDefaults.Size = new System.Drawing.Size(191, 18);
            this.labelShelfDefaults.TabIndex = 17;
            this.labelShelfDefaults.Text = "Change All Shelf Orders To:";
            this.labelShelfDefaults.Visible = false;
            // 
            // comboBoxShelfDefaults
            // 
            this.comboBoxShelfDefaults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxShelfDefaults.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxShelfDefaults.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxShelfDefaults.FormattingEnabled = true;
            this.comboBoxShelfDefaults.Location = new System.Drawing.Point(743, 46);
            this.comboBoxShelfDefaults.Name = "comboBoxShelfDefaults";
            this.comboBoxShelfDefaults.Size = new System.Drawing.Size(233, 28);
            this.comboBoxShelfDefaults.TabIndex = 18;
            this.comboBoxShelfDefaults.Visible = false;
            this.comboBoxShelfDefaults.SelectedIndexChanged += new System.EventHandler(this.comboBoxShelfDefaults_SelectedIndexChanged);
            // 
            // checkBoxRacks
            // 
            this.checkBoxRacks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxRacks.AutoSize = true;
            this.checkBoxRacks.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBoxRacks.Location = new System.Drawing.Point(797, 15);
            this.checkBoxRacks.Name = "checkBoxRacks";
            this.checkBoxRacks.Size = new System.Drawing.Size(179, 24);
            this.checkBoxRacks.TabIndex = 19;
            this.checkBoxRacks.Text = "Show Rack Number";
            this.checkBoxRacks.UseVisualStyleBackColor = true;
            this.checkBoxRacks.Visible = false;
            this.checkBoxRacks.CheckedChanged += new System.EventHandler(this.checkBoxRacks_CheckedChanged);
            // 
            // labelShelfOrder
            // 
            this.labelShelfOrder.AutoSize = true;
            this.labelShelfOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShelfOrder.Location = new System.Drawing.Point(12, 96);
            this.labelShelfOrder.Name = "labelShelfOrder";
            this.labelShelfOrder.Size = new System.Drawing.Size(87, 18);
            this.labelShelfOrder.TabIndex = 20;
            this.labelShelfOrder.Text = "Shelf Order:";
            this.labelShelfOrder.Visible = false;
            // 
            // comboBoxSiteDefaults
            // 
            this.comboBoxSiteDefaults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSiteDefaults.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSiteDefaults.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxSiteDefaults.FormattingEnabled = true;
            this.comboBoxSiteDefaults.Location = new System.Drawing.Point(743, 92);
            this.comboBoxSiteDefaults.Name = "comboBoxSiteDefaults";
            this.comboBoxSiteDefaults.Size = new System.Drawing.Size(233, 28);
            this.comboBoxSiteDefaults.TabIndex = 21;
            this.comboBoxSiteDefaults.Visible = false;
            this.comboBoxSiteDefaults.SelectedIndexChanged += new System.EventHandler(this.comboBoxSiteDefaults_SelectedIndexChanged);
            // 
            // labelSiteDefaults
            // 
            this.labelSiteDefaults.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelSiteDefaults.AutoSize = true;
            this.labelSiteDefaults.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSiteDefaults.Location = new System.Drawing.Point(478, 96);
            this.labelSiteDefaults.Name = "labelSiteDefaults";
            this.labelSiteDefaults.Size = new System.Drawing.Size(225, 18);
            this.labelSiteDefaults.TabIndex = 22;
            this.labelSiteDefaults.Text = "Change Selected Shelf Order To:";
            this.labelSiteDefaults.Visible = false;
            // 
            // toolTipHelp
            // 
            this.toolTipHelp.AutomaticDelay = 0;
            this.toolTipHelp.AutoPopDelay = 5000;
            this.toolTipHelp.InitialDelay = 250;
            this.toolTipHelp.ReshowDelay = 100;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(989, 277);
            this.Controls.Add(this.labelSiteDefaults);
            this.Controls.Add(this.comboBoxSiteDefaults);
            this.Controls.Add(this.labelShelfOrder);
            this.Controls.Add(this.checkBoxRacks);
            this.Controls.Add(this.comboBoxShelfDefaults);
            this.Controls.Add(this.labelShelfDefaults);
            this.Controls.Add(this.textBoxShelfOrder);
            this.Controls.Add(this.OutFileButton);
            this.Controls.Add(this.comboBoxSiteNames);
            this.Controls.Add(this.labelSiteName);
            this.Controls.Add(this.GenLabelButton);
            this.Controls.Add(this.textBoxOutputFile);
            this.Controls.Add(this.textBoxInputFile);
            this.Controls.Add(this.InFileButton);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Optical Team Label Generator REMIX v1.21";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button InFileButton;
        private System.Windows.Forms.Button OutFileButton;
        private System.Windows.Forms.TextBox textBoxInputFile;
        private System.Windows.Forms.TextBox textBoxOutputFile;
        private System.Windows.Forms.Button GenLabelButton;
        private System.Windows.Forms.Label labelSiteName;
        private System.Windows.Forms.ComboBox comboBoxSiteNames;
        private System.Windows.Forms.TextBox textBoxShelfOrder;
        private System.Windows.Forms.Label labelShelfDefaults;
        private System.Windows.Forms.ComboBox comboBoxShelfDefaults;
        private System.Windows.Forms.CheckBox checkBoxRacks;
        private System.Windows.Forms.Label labelShelfOrder;
        private System.Windows.Forms.ComboBox comboBoxSiteDefaults;
        private System.Windows.Forms.Label labelSiteDefaults;
        private System.Windows.Forms.ToolTip toolTipHelp;
    }
}

